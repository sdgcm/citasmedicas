package com.kjquito.sdgcm.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.kjquito.sdgcm.models.entity.Citas;

public interface CitasDao extends CrudRepository<Citas, Long> {
	/*@Query("SELECT fecha FROM citas ORDER BY fecha DESC")
	Iterable<Citas> findAll();*/
}
