package com.kjquito.sdgcm.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RequestParam;

import com.kjquito.sdgcm.models.entity.Citas;
import com.kjquito.sdgcm.models.entity.Paciente;

public interface ICitas {

	//public Iterable<Citas> findAll();

	public Optional<Citas> encontrarid(Long id);

	public Citas save(Citas citas);

	public void deleteById(Long id);

	public List<Paciente> obtenerPacientePorCita(@RequestParam Iterable<Long> ids);
	
	//@Query("SELECT fecha FROM citas ORDER BY fecha DESC")
	public Iterable<Citas> findAll();
	
	public Iterable<Citas> findAllById(Iterable<Long> ids);

}
