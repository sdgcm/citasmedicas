package com.kjquito.sdgcm.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.kjquito.sdgcm.client.CitasFeingClient;
import com.kjquito.sdgcm.models.dao.CitasDao;
import com.kjquito.sdgcm.models.entity.Citas;
import com.kjquito.sdgcm.models.entity.Paciente;

@Service
public class CitasServiceImplement implements ICitas {

	@Autowired
	private CitasDao citasDao;
	
	@Autowired
	private CitasFeingClient citasClient;

	/*@Override
	@Transactional(readOnly = true)
	public List<Citas> findAll() {
		return (List<Citas>) recetasDao.findAll();
	}*/
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Citas> encontrarid(Long id) {
		return citasDao.findById(id);
	}

	/*@Override
	@Transactional(readOnly = true)
	public Citas findById(Long id) {
		return recetasDao.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
				String.format("ID  de receta inavlido %s", id)));
	}*/

	@Override
	public Citas save(Citas citas) {
		return citasDao.save(citas);
	}

	@Override
	public void deleteById(Long id) {
		citasDao.deleteById(id);
	}

	@Override
	public Iterable<Citas> findAll() {
		return  citasDao.findAll();
	}

	@Override
	public List<Paciente> obtenerPacientePorCita(Iterable<Long> ids) {
		return (List<Paciente>) citasClient.obtenerPacientePorCita(ids);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Citas> findAllById(Iterable<Long> ids) {
		
		return citasDao.findAllById(ids);
	}
	
	

	





	

}
