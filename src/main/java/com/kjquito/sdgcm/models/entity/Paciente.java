package com.kjquito.sdgcm.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@Entity
//@Table(name = "paciente")
//@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Paciente implements Serializable {
	
	/*@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)*/
	private Long Id;

	//@Column(name = "identificacion")
	private String identificacion;

	//@Column(name = "nombres")
	private String nombres;

	//@Column(name = "apellidos")
	private String apellidos;

	//@Column(name = "edad")
	private String edad;

	//@Column(name = "fecha_nacimiento")
	private String fecha_nacimiento;

	@Column(name = "direccion")
	private String direccion;

	//@Column(name = "telefono")
	private String telefono;

	//@Column(name = "alergia")
	private String alergia;	
	

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getAlergia() {
		return alergia;
	}

	public void setAlergia(String alergia) {
		this.alergia = alergia;
	}
	



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
