package com.kjquito.sdgcm.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "citas")
public class Citas implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "fecha")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;

	@Column(name = "descripcion")
	private String descripcion;

	@JsonIgnoreProperties(value = { "citas" })
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "citas", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CitasPaciente> citasPaciente;

	@Transient
	private List<Paciente> paciente;
	
	public Citas() {
		this.paciente = new ArrayList<>();
		this.citasPaciente = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<CitasPaciente> getCitasPaciente() {
		return citasPaciente;
	}

	public void setCitasPaciente(List<CitasPaciente> citasPaciente) {
		this.citasPaciente = citasPaciente;
	}

	public List<Paciente> getPaciente() {
		return paciente;
	}

	public void setPaciente(List<Paciente> paciente) {
		this.paciente = paciente;
	}
	
	public void addPaciente(Paciente paciente) {
		this.paciente.add(paciente);
	}
	
	public void rermovePaciente(Paciente paciente) {
		this.paciente.remove(paciente);
	}
	
	public void addCitasPaciente(CitasPaciente citasPaciente) {
		this.citasPaciente.add(citasPaciente);
	}
	
	public void removeCitasPaciente(CitasPaciente citasPaciente) {
		this.citasPaciente.remove(citasPaciente);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8086050399388593423L;

}
