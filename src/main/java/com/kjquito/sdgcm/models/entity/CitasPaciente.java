package com.kjquito.sdgcm.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "citas_Pacientes")
public class CitasPaciente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "paciente_id")
	private Long paciente_Id;
	
	@JsonIgnoreProperties(value = {"citasPaciente"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "citas_id")
	private Citas citas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPaciente_Id() {
		return paciente_Id;
	}

	public void setPaciente_Id(Long paciente_Id) {
		this.paciente_Id = paciente_Id;
	}

	public Citas getCitas() {
		return citas;
	}

	public void setCitas(Citas citas) {
		this.citas = citas;
	}
}
