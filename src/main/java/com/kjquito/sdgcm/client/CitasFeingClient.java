package com.kjquito.sdgcm.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kjquito.sdgcm.models.entity.Paciente;

@FeignClient(name = "servicio-pacientes", url= "http://ec2-34-205-89-145.compute-1.amazonaws.com")
public interface CitasFeingClient {
	@GetMapping("/sdgcm/api/v1/pacientes-by-cita")
	public Iterable<Paciente> obtenerPacientePorCita(@RequestParam Iterable<Long> ids);
}
