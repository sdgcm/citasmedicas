package com.kjquito.sdgcm.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.kjquito.sdgcm.models.entity.Citas;
import com.kjquito.sdgcm.models.entity.CitasPaciente;
import com.kjquito.sdgcm.models.entity.Paciente;

import com.kjquito.sdgcm.models.service.ICitas;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
@RequestMapping("/sdgcm/api/v1")
public class CitasController {

	@Autowired
	private ICitas citasService;

	/*@GetMapping("/citas")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> obtenerPacientes() {
		return ResponseEntity.ok(citasService.findAll());
	}*/
	
	@GetMapping("/citas")
	public ResponseEntity<?> obtenerPacientes() {
		List<Citas> citas = ((List<Citas>) citasService.findAll()).stream().map(m ->{
			m.getCitasPaciente().forEach(mu ->{
				Paciente paciente = new Paciente();
				paciente.setId(mu.getPaciente_Id());
				m.addPaciente(paciente);
			});
			return m;
		}).collect(Collectors.toList());
		return ResponseEntity.ok().body(citas);
	}
	
	@GetMapping("/citas-por-medico")
	public ResponseEntity<?> obtenerCitasPorMedico(@RequestParam List<Long> ids){
		return ResponseEntity.ok(citasService.findAllById(ids));
	}
	
	@GetMapping("/citas/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> listarids(@PathVariable Long id) {
		Optional<Citas> om = citasService.encontrarid(id);
		if(!om.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Citas citas = om.get();
		if(citas.getCitasPaciente().isEmpty()== false) {
			List<Long> ids = citas.getCitasPaciente().stream().map(mu ->{
				return mu.getPaciente_Id();
			}).collect(Collectors.toList());
			
			List<Paciente> pacientes = citasService.obtenerPacientePorCita(ids);
			citas.setPaciente(pacientes);
		}
		return ResponseEntity.ok().body(citas);
	}

	@PostMapping("/citas")
	@ResponseStatus(HttpStatus.CREATED)
	public Citas save(@RequestBody Citas citas) {
		return citasService.save(citas);
	}

	@PutMapping("/citas/{id}")
	public ResponseEntity<?> modificar(@RequestBody Citas citas, @PathVariable Long id) {
		Optional<Citas> citas2 = citasService.encontrarid(id);
		if (!citas2.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Citas citas3 = citas2.get();
		citas3.setFecha(citas.getFecha());
		citas3.setDescripcion(citas.getDescripcion());

		return ResponseEntity.status(HttpStatus.CREATED).body(citasService.save(citas3));
	}

	@DeleteMapping("/citas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable Long id) {
		citasService.deleteById(id);
	}

	
	@PutMapping("citas/{id}/asignar-paciente")
	public ResponseEntity<?> asignarCitaPaciente(@RequestBody Paciente paciente, @PathVariable Long id) {
		Optional<Citas> opt = citasService.encontrarid(id);
		if (!opt.isPresent()) {

			return ResponseEntity.notFound().build();
		}
		Citas dbCitas = opt.get();
		CitasPaciente citasPaciente = new CitasPaciente();
		//paciente.forEach(a -> {
			citasPaciente.setPaciente_Id(paciente.getId());
			citasPaciente.setCitas(dbCitas);
			dbCitas.addCitasPaciente(citasPaciente);	
		//});

		return ResponseEntity.status(HttpStatus.CREATED).body(citasService.save(dbCitas));

	}
	

}
